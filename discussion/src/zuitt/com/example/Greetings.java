package zuitt.com.example;

public interface Greetings {

    public void morningGreetings();

    public void holidayGreetings();
}
