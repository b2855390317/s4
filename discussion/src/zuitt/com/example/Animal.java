package zuitt.com.example;

//animal class will serve as the parent class
public class Animal {

    //properties
    private String name;
    private String color;

    //constructor

    public Animal(){}

    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }

    public String getName(){
        return this.name;
    }

    public String getColor() {
        return color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    //method

    public void call(){
        System.out.println("Hi my name is " + this.name + "! (From Animal.java)");
    }
}
