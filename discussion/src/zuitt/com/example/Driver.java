package zuitt.com.example;

public class Driver {

    //it is another component or composition that is needed for


    private String name;

    public Driver(){}

    public Driver(String name){
        this.name = name;
    }

    //get and set

    public String getName(){
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
