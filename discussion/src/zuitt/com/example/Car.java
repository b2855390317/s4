package zuitt.com.example;

public class Car {


    //Access Modifier
        //these are used to restrict the scope of a class, constructor, variable, method or data
    //Four types of access Modifiers
    //1. Default - no keyword (accessibility is within the package)
    //2. Private - properties or method are only accessible within the class
        //example - within Car.java
    //3. Protected - properties and methods are only accessible by the class of the same package and the subclass
    //4. Public - properties and methods can be accessed anywhere

    //Class Creation
    //1. Properties - characteristic of objects

    private String name;
    private String brand;
    private int yearOfMake;

    //2. Constructor is used to create/instantiate an object

    //a. empty constructor- creates an object that doesn't have any arguments or parameters

    //also known as the default constructor

    public Car(){
        //set a default value upon instantiation
        this.yearOfMake = 2000;

        //added driver
        this.driver = new Driver("Badong");
    }

    //b. parameterized constructor - creates an object with arguments/ parameters

    public Car(String name,String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Badong");
    }

    //3. Getters and setters
        //get and set the values of each property of an object
    //getters - retrieve the value of an instantiated object

    public String getName(){
        return this.name;
    }

    public String getBrand(){
        return this.brand;
    }

    public int getYearOfMake(){
        return  this.yearOfMake;
    }

    public String getDriverName(){
        return this.driver.getName();
    }

    // make a driver component of the car

    private Driver driver;

    //b. setters - used to change the default value of instantiated object

    // void - we don't need to return anything

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake) {

        if (yearOfMake <= 2023) {
            this.yearOfMake = yearOfMake;
        }
    }

    public void  setDriver(String driver){
        this.driver.setName(driver);
    }

    //4. methods - functions an object can perform

    public void drive(){
        System.out.println("The car is running. Vroom! VrooM! Skrt skrt!");
    }



}
