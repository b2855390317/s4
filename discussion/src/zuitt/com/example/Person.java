package zuitt.com.example;

public class Person implements Actions, Greetings{
    @Override
    public void sleep() {
        System.out.println("zzzzzzZ!");
    }

    public void run(){
        System.out.println("running!!");
    }

    @Override
    public void morningGreetings() {
        System.out.println("Good morning minna-san!");
    }

    @Override
    public void holidayGreetings() {
        System.out.println("Merīkurisumas!");
    }
}
