package zuitt.com.example;
//Child class of animal class
//extends is used to inherit the properties and methods of the parents
public class Dog extends Animal{

    //properties

    private String breed;

    //constructor

    public Dog(){
        //super - direct access with the original constructor to inherit
        super();
        this.breed = "American bully";
    }

    public Dog(String name, String color, String breed){

        super(name, color);
        this.breed = breed;
    }

    //getter and setter


    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    //method

    public void speak(){
        System.out.println("rawr! rawr!");
    }

    @Override
    public void call() {
        //we have direct access to parent method

        super.call();
        System.out.println(" Hi, I'm " + this.getName() + ". I am a dog! (from Dog.java)");
    }


}
