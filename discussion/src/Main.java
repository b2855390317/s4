import zuitt.com.example.Car;
import zuitt.com.example.Dog;
import zuitt.com.example.Person;

public class Main {
    public static void main(String[] args) {


        //OOP - stands for Object-Oriented Programming is a programming model that allows developers to design software around data or objects rather than function and logic
        //OOP Concepts
            //Objects - abstract idea that represents something in the real world
            //Example: the concept of a dog
            //Class - representation of the object using code
            //Example: Writing a code that would describe a dog
            //Instance - unique copy of the idea, made "physical"
                //actual creation or the physical copy
                // enables us to copy the concept such as  an instance
            // Example: Instantiating a dog named Fluffy from the dog class

        //Objects
            //"States and Attributes" - what is the idea about?
            // Properties of the specific object
            // Dog's name, breed , color etc...
            //Behaviors - what can the idea do?
                //what can the actual dog do?
                // bark and sit

        //Four Pillars of OOP
            //1. Encapsulation- a mechanism of wrapping the data (variables and code acting on the data (methods) together as a single unit
            //"data hiding" - the variables of a class will be hidden from the other classes, and can be accessed only through the methods of their current class
            // to achieve, encapsulation, these are some examples:
                //variable/properties as private
                // provide a public setter and getter function


        //create a car

        Car myCar = new Car(); // we accessed the empty /default constructor
        myCar.drive();

        // assign the properties of myCar using setter method
        myCar.setName("HiAce");
        myCar.setBrand("Toyota");
        myCar.setYearOfMake(2023);



        //composition and inheritance

        //inheritance it allows modelling an object that is a subset of another object
            //it defines "is a relationship"
        //composition - allows modelling objects that are made up of other objects
            // both entities are dependent on each other objects
            //compose object cannot exist without the other entity
        //it defines " has a relationship"

        //example
            //A car is a vehicle - inheritance
            // a car has a driver - composition



        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ".  It is driven by " + myCar.getDriverName());

        myCar.setDriver("Dodong");

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ".  It is driven by " + myCar.getDriverName());

        //System.out.println("Hello world!");

        //2. Inheritance
        //can be defined as the process where one class acquire the properties and methods of another class
        //with the use of inheritance, the information is made manageable in hierarchical order


        Dog myPet = new Dog();
        myPet.setName("bantay");
        myPet.setColor("black");
        myPet.speak();

        System.out.println("My dog is " +myPet.getName() + " and it is a " + myPet.getColor()+ " " + myPet.getBreed()+ "!");

        myPet.call();


        //3. Abstaction



        Person child = new Person();
        child.sleep();
        child.morningGreetings();
        child.holidayGreetings();

    }
}